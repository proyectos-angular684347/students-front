# Estudents-front

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 17.0.3.

## Development install

Run `npm install` to install the packages necessary for the correct functioning of the app

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Description

Test project in Angular 17, basic student crud.

Individualized project in components, implementation of the following embodiment

![Alt text](image.png)

### Core

Main base of the project, initial component of the project, next to it are the main views and the essential tools and procedures for the start of the app.

### Components

Comprende los diferentes componentes y logica de la app

### Services

Connection services to third parties such as project API's or third parties.

### Interfaces

Different project interfaces for the respective code typing to ensure good development practices.

### Store

[@ngrx/store](https://ngrx.io/guide/store) The project counts with the implementation of the redux pattern, to control the connections to other services and maintain the data in a more efficient way.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
