import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { Store } from '@ngrx/store';
import { InitialStateStudent } from './../store/student/student.reducer';
import { getStudents } from '../store/student/student.actions';
import { StudentService } from '../services/student.service';

@Component({
  selector: 'app-core',
  standalone: true,
  imports: [CommonModule, RouterOutlet, HeaderComponent, SidebarComponent],
  templateUrl: './core.component.html',
  styleUrl: './core.component.scss',
})
export default class CoreComponent implements OnInit {
  constructor(
    private store: Store<InitialStateStudent>,
    private service: StudentService
  ) {}

  ngOnInit() {
    this.store.dispatch(getStudents());
  }
}
