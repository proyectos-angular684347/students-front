import { Routes } from '@angular/router';

export const routes: Routes = [
  {
    path: '',
    loadComponent: () => import('./core/core.component'),
    children: [
      {
        path: 'students',
        title: 'Estudiantes',
        loadComponent: () =>
          import('./components/student-list/student-list.component'),
      },
      {
        path: 'students-add',
        title: 'Detalle estudiante',
        loadComponent: () =>
          import('./components/add-student/add-student.component'),
      },
      {
        path: 'students-detail/:id',
        title: 'Detalle estudiante',
        loadComponent: () =>
          import('./components/student-detail/student-detail.component'),
      },
      {
        path: '',
        redirectTo: 'students',
        pathMatch: 'full',
      },
    ],
  },
];
