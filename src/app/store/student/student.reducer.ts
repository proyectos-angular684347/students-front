import { createReducer, on } from '@ngrx/store';
import { Student } from '../../interfaces/student.interface';
import {
  addStudents,
  deleteStudents,
  getStudentsId,
  getStudentsSuccess,
  updateStudents,
} from './student.actions';

export interface InitialStateStudent {
  students: Student[];
  studentDetail: Partial<Student>;
}

export const initialStateStudent: InitialStateStudent = {
  students: [],
  studentDetail: {},
};

export const studentReducer = createReducer(
  initialStateStudent,
  on(getStudentsSuccess, (state, action) => {
    return {
      ...state,
      students: action.payload,
    };
  }),
  on(deleteStudents, (state, action) => {
    const newStudents: Student[] = JSON.parse(JSON.stringify(state.students));
    const indexDelete = state.students.findIndex(
      (std) => std.id === action.payload
    );
    newStudents.splice(indexDelete, 1);
    console.log(state.students, indexDelete);
    return {
      ...state,
      students: newStudents,
    };
  }),
  on(addStudents, (state, action) => {
    const newStudents: Student[] = JSON.parse(JSON.stringify(state.students));
    const student = JSON.parse(JSON.stringify(action.payload));
    student.id = Math.floor(newStudents.length + Math.random() * 5);
    newStudents.push(student);
    return {
      ...state,
      students: newStudents,
    };
  }),
  on(updateStudents, (state, action) => {
    const newStudents: Student[] = JSON.parse(JSON.stringify(state.students));
    const index = state.students.findIndex(
      (std) => std.id === action.payload.id
    );
    newStudents[index] = action.payload.data;
    return {
      ...state,
      students: newStudents,
      studentDetail: {},
    };
  }),
  on(getStudentsId, (state, action) => {
    const newStudents: Student[] = JSON.parse(JSON.stringify(state.students));
    const index = state.students.findIndex((std) => std.id === action.payload);
    return {
      ...state,
      studentDetail: newStudents[index],
    };
  })
);
