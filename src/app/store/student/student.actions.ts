import { createAction, props } from '@ngrx/store';
import { Student } from '../../interfaces/student.interface';

export const getStudents = createAction('[Student] Get Students');
export const getStudentsSuccess = createAction(
  '[Student] Get Students success',
  props<{ payload: Student[] }>()
);

export const getStudentsId = createAction(
  '[Student] Get Students id',
  props<{ payload: number }>()
);

export const addStudents = createAction(
  '[Student] add Students',
  props<{ payload: Student }>()
);

export const updateStudents = createAction(
  '[Student] update Students',
  props<{ payload: { data: Student; id: number } }>()
);

export const deleteStudents = createAction(
  '[Student] delete Students',
  props<{ payload: number }>()
);
