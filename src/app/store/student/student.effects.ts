import { Injectable, inject } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { StudentService } from '../../services/student.service';
import { getStudents, getStudentsSuccess } from './student.actions';
import { catchError, map, mergeMap, of } from 'rxjs';

@Injectable()
export class StudentEffects {
  private sService: StudentService = inject(StudentService);
  constructor(private actions$: Actions) {}

  getStudents$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(getStudents),
      mergeMap(() =>
        this.sService.getClientes().pipe(
          map((res) => getStudentsSuccess({ payload: res })),
          catchError((_error) => of(getStudentsSuccess({ payload: [] })))
        )
      )
    );
  });
}
