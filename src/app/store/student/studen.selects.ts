import { createSelector, createFeatureSelector } from '@ngrx/store';
import { Student } from '../../interfaces/student.interface';

export const featureKey = 'student';

export interface FeatureState {
  students: Student[];
  studentDetail: Student;
}

export const selectFeature = createFeatureSelector<FeatureState>(featureKey);

export const selectStudents = createSelector(
  selectFeature,
  (state) => state.students
);

export const selectStudentDetail = createSelector(
  selectFeature,
  (state) => state.studentDetail
);
