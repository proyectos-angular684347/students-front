import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Student } from '../interfaces/student.interface';

@Injectable({
  providedIn: 'root',
})
export class StudentService {
  private service = 'https://jsonplaceholder.typicode.com/';

  constructor(private http: HttpClient) {}

  getClientes(): Observable<Student[]> {
    return this.http.get<Student[]>(this.service + 'users');
  }
}
