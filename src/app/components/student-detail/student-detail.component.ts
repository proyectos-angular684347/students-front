import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  getStudentsId,
  updateStudents,
} from '../../store/student/student.actions';
import { Student } from '../../interfaces/student.interface';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { Store } from '@ngrx/store';
import { InitialStateStudent } from '../../store/student/student.reducer';
import {
  FormGroup,
  FormControl,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { selectStudentDetail } from '../../store/student/studen.selects';

@Component({
  selector: 'app-student-detail',
  standalone: true,
  imports: [CommonModule, RouterModule, FormsModule, ReactiveFormsModule],
  templateUrl: './student-detail.component.html',
})
export default class StudentDetailComponent implements OnInit {
  id: number = -1;

  form: FormGroup = new FormGroup({
    name: new FormControl(null, Validators.required),
    user: new FormControl(null, Validators.required),
    email: new FormControl(null, Validators.required),
    phone: new FormControl(null, Validators.required),
  });

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<InitialStateStudent>
  ) {}

  ngOnInit() {
    this.id = Number(this.route.snapshot.paramMap.get('id'));
    this.store.dispatch(getStudentsId({ payload: this.id }));
    this.store.select(selectStudentDetail).subscribe((student) => {
      this.form.setValue({
        name: student.name,
        user: student.username,
        email: student.email,
        phone: student.phone,
      });
    });
  }

  onNavigateBack() {
    this.form.reset();
    this.router.navigateByUrl('/students');
  }

  onSubmit() {
    if (this.form.valid) {
      const student: Student = {
        id: -1,
        website: '',
        name: this.form.value.name,
        username: this.form.value.user,
        email: this.form.value.email,
        phone: this.form.value.phone,
        address: {
          city: '',
          street: '',
          suite: '',
          zipcode: '',
          geo: {
            lat: '',
            lng: '',
          },
        },
        company: {
          bs: '',
          catchPhrase: '',
          name: '',
        },
      };
      this.store.dispatch(
        updateStudents({ payload: { data: student, id: this.id } })
      );
      this.onNavigateBack();
    }
  }
}
