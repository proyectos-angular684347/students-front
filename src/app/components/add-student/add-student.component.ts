import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router } from '@angular/router';
import {
  FormGroup,
  FormControl,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { Store } from '@ngrx/store';
import { InitialStateStudent } from '../../store/student/student.reducer';
import { Student } from '../../interfaces/student.interface';
import { addStudents } from '../../store/student/student.actions';

@Component({
  selector: 'app-add-student',
  standalone: true,
  imports: [CommonModule, FormsModule, ReactiveFormsModule],
  templateUrl: './add-student.component.html',
})
export default class AddStudentComponent {
  form: FormGroup = new FormGroup({
    name: new FormControl(null, Validators.required),
    user: new FormControl(null, Validators.required),
    email: new FormControl(null, Validators.required),
    phone: new FormControl(null, Validators.required),
  });

  constructor(
    private router: Router,
    private store: Store<InitialStateStudent>
  ) {}

  onNavigateBack() {
    this.form.reset();
    this.router.navigateByUrl('/students');
  }

  onSubmit() {
    if (this.form.valid) {
      const student: Student = {
        id: -1,
        website: '',
        name: this.form.value.name,
        username: this.form.value.user,
        email: this.form.value.email,
        phone: this.form.value.phone,
        address: {
          city: '',
          street: '',
          suite: '',
          zipcode: '',
          geo: {
            lat: '',
            lng: '',
          },
        },
        company: {
          bs: '',
          catchPhrase: '',
          name: '',
        },
      };
      this.store.dispatch(addStudents({ payload: student }));
      this.onNavigateBack();
    }
  }
}
