import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Store } from '@ngrx/store';
import { InitialStateStudent } from '../../store/student/student.reducer';
import { selectStudents } from '../../store/student/studen.selects';
import { Student } from '../../interfaces/student.interface';
import { deleteStudents } from '../../store/student/student.actions';
import { Router } from '@angular/router';

@Component({
  selector: 'app-student-list',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './student-list.component.html',
})
export default class StudentListComponent implements OnInit {
  public students: Student[] = [];

  constructor(
    private store: Store<InitialStateStudent>,
    private router: Router
  ) {}

  ngOnInit() {
    this.store
      .select(selectStudents)
      .subscribe((students) => (this.students = students));
  }

  onDeleteStudent(id: number) {
    this.store.dispatch(deleteStudents({ payload: id }));
  }

  onNavigateAddStudent() {
    this.router.navigateByUrl('/students-add');
  }

  onNavigateUpdateStudent(id: number) {
    this.router.navigateByUrl('/students-detail/' + id);
  }
}
